# Java 1.8 Features
JAVA 8 is a major feature release of JAVA programming language development. Its initial version was released on 18 March 2014.
With the Java 8 release, Java provided supports for functional programming, new JavaScript engine, new APIs for date time manipulation, new streaming API, etc.

## New Features
### forEach() method in Iterable interface:

Prior to java 8 the three most common ways to iterate through a collection are by using the while loop, for loop, and enhanced for loop.
As the Java Collection interface extends Iterable, you can also use the hasNext() and next() methods of Iterable to iterate through collection elements.
 - Java 8 introduced forEach method in Iterable interface to loop through elements in a collection – but in a different way.




### default and static methods in Interfaces:

### Functional Interfaces and Lambda Expressions:
**Lambda Function:** Lambda function is anonymous functon. Anonymous function is a function with no name, no return type and no access modifier.
 - Note: This type of interface, was previously known as a Single Abstract Method type (SAM).

Lambda function introduced in java 8 to enable functional programming support.

Lambdas should provide a means to better support the Don't Repeat Yourself (DRY) principle and make your code simpler and more readable.


*-> is notation of lambda*

Conversion of some simple methods into Lambda Function:

##### Example 1:

```
public void greetHello(){
   System.out.println("Hello World"); 
}
```
In Lambda above code can be written as
```
() -> { System.out.println("Hello World"); }
```

##### Example 2:

```
public void add(int a,int b){
    System.out.println(a+b);
}
```
In Lambda above code can be written as
```
(int a,int b) -> System.out.println(a+b);
```
Enhanced Lambda Code of above code
```
(a,b) -> System.out.println(a+b);
```
##### Example 3:

```
public int square(int a){
    return a*a;
}
```
In Lambda above code can be written as
```
a -> a*a;
```

##### Lambda function Rules :

**1.** Lambda can have any number of arguments, you can have 0, 1, 2 .... n number of arguments in lambda function.

**2.** For one-argument lambda expession parenthesis are optional. **Example : a -> a*a;**

**3.** For zero argument or more than one argument parenthesis must required. **Example : (a,b) -> System.out.println(a+b);**

**4.** Curly braces is optional for one liner code but if you have more than one lice of code then curly braces must be written. 
       **Example : () -> System.out.println("Hello World");   OR   () -> { System.out.println("Hello World"); }

**5.** Functional Interface must reuired to call lambda function.

*A functional interfae is an interface with restrict to only one abstract method declaration. Functional Interface can have one or more default and  statis method but only one abstract method. You can create functional interface using @FunctionalInterface, but this annotation is not mandatory to create Functional Interface*

**6.** The type of the parameters can be explicitly declared or it can be inferred from the context.

**7.** The body of the lambda expression can contain zero, one or more statements.

**8.** When there is a single statement curly brackets are not mandatory and the return type of the anonymous function is the same as that of the body expression.

**9.** When there is more than one statement then these must be enclosed in curly brackets (a code block) and the return type of the anonymous function is the same as the type of the value returned within the code block, or void if nothing is returned.

**10.** The lambda expressions are represented as objects, and so they must be bound to a particular object type known as a functional interface. This is called the target type.

*See the Java documentation for the new **java.util.function** package to get more details on these functional interfaces and their corresponding abstract methods.*

*Notice that the type of the lambda expression is determined by the compiler from the context based on the target type. This implies that two apparently equal lambda expressions may have different types simply because they are bound to a different target type as demonstrated in the following code.*

```
Callable<String> callMe = () -> "Hello";
PrivilegedAction<String> action = () -> "Hello"
```

#### Few Lambda examples for understanding:

**Q1.** Given a number n returns a boolean indicating if it is odd.
```
n -> n % 2 != 0;
```

**Q2.** Given a character c returns a boolean indicating if it is equal to ‘y’.
```
(char c) -> c == 'y';
```

**Q3.** Given two numbers x and y returns another number with their summation.
```
(x, y) -> x + y;
```

**Q4.** Given two integers a and b returns another integer with the sum of their squares.
```
(int a, int b) -> a * a + b * b
```

**Q5.** Given no parameters returns the integer 42.
```
n -> 42;
```

**Q6.** Given no parameters returns the double 3.14.

```
() -> { return 3.14 };
```

**Q7.** Given a sting s prints the string to the main output and returns void.
```
(String s) -> { System.out.println(s); };
```

**Q8.** Give no parameters prints Hello World! to the main output and returns void.
```
() -> { System.out.println("Hello World!"); };
```

#### FAQ's about Lambda Function

**Q : What is a lambda expression?**

Lambda expressions in Java introduce the idea of functions into the language. Lambda function is anonymous method with a more compact syntax that also allows the omission of modifiers, return type, and in some cases parameter types as well.

**Q : Why are lambda expressions being added to Java?**

Currently, lists and sets are typically processed by client code obtaining an iterator from the collection, then using that to iterate over its elements and process each in turn. If the processing of different elements is to proceed in parallel, it is the responsibility of the client code, not the collection, to organise this.
In Java 8, the intention is instead to provide collections with methods that will take functions and use them, each in different ways, to process their elements.
We will use as an example the very simple method **forEach**, which takes a function and just applies it to every element.
The advantage that this change brings is that collections can now organise their own iteration internally, transferring responsibility for parallelisation from client code into library code.

However, for client code to take advantage of this, there needs to be a simple way of providing a function to the collection methods. Currently the standard way of doing this is by means of an anonymous class implementation of the appropriate interface. But the syntax for defining anonymous inner classes is too clumsy to be practicable for this purpose.
For example, the forEach method on collections will take an instance of the Consumer interface and call its accept method for every element:

```
interface Consumer<T> { void accept(T t); } 
```
Suppose that we want to use forEach to transpose the x and y co-ordinates of every element in a list of java.awt.Point. Using an anonymous inner class implementation of Consumer we would pass in the transposition function like this:

```
pointList.forEach(new Consumer<Point>() { 
        public void accept(Point p) { 
            p.move(p.y, p.x);
        } 
});
```
Using a lambda, however, the same effect can be achieved much more concisely:
```
pointList.forEach(p -> p.move(p.y, p.x)); 
```
**Q :Are lambda expressions objects?**

A lambda expression is an instance of a functional interface, which is itself a subtype of Object. To see this, consider the legal assignments:
```
Runnable r = () -> {};   // creates a lambda expression and assigns a reference to this lambda to r 
```

**Q :Where can lambda expressions be used?**

Lambda expressions can be written in any context that has a target type. The contexts that have target types are:
 - Variable declarations and assignments and array initializers, for which the target type is the type (or the array type) being assigned to;
 - Return statements, for which the target type is the return type of the method;

**Q :Can lambda expressions be used to define recursive functions?**

Yes, provided that the recursive call uses a name defined in the enclosing environment of the lambda.

Exaple
```
UnaryOperator<Integer> factorial = i -> i == 0 ? 1 : i * factorial.apply( i - 1 );
```

**Q :What are method references?**

Method references are a feature of Java 8. They are effectively a subset of lambda expressions, because if a lambda expression can be used, then it might be possible to use a method reference, but not always. They can only be used to call a singular method.
It would be a good idea if you knew the notation for a method reference. In fact, you have probably already seen it assuming you read the title. If not then just look below.
```
Person::getName 
```
##### Types of Method References

**1.Reference to a static method:**
    
  **Syntax** Class::staticMethod
    
  **Method Reference** String::valueOf
    
  **Lambda expression**  s -> String.valueOf(s)
  
  **Example**
```
  public class StaticMethodReference {

	public static void main(String[] args) {
		
		List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		//Method Reference
		list.forEach(StaticMethodReference::print);
		//Lambda Reference
		list.forEach(number -> StaticMethodReference.print(number));
		//Normal Flow
		for	(int number : list){
			StaticMethodReference.print(number);
		}
	}
	
	public static void print(final int number) {
		System.out.println("I am printing "+number);
	}
}
```

In the above calss  static method StaticMethodReference.print. This example is pretty simple. There is a static method, and for each element in the list, it calls this method using the element as the input.


**2.Reference to an instance method of a particular object:**
    
  **Syntax** instance::instanceMethod
    
  **Method Reference** s:toString
    
  **Lambda expression**   () -> “string”.toString()

**Example**

```
public class InstanceMethodReference {
	public static void main(String[] args) {
		final List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		final MyComparator myComparator=new MyComparator();
		//Method Reference
		Collections.sort(list,myComparator::compare);			
		//Lambda Reference
		Collections.sort(list, (a,b) -> myComparator.compare(a, b));
	}

	private static class MyComparator{
		public int compare(final Integer n1,final Integer n2) {
			return n1.compareTo(n2);
		}
	}
	
}
```

Here, it calls the instance method myComparator.compare, where myComparator is a particular instance of MyComparator.


**3.Reference to an instance method of an arbitrary object of a particular type	:**
    
  **Syntax** Class:instanceMethod
    
  **Method Reference** String::toString	
    
  **Lambda expression**   s -> s.toString()

**Example**

```
public class ArbitraryInstaneMethodRef {

	public static void main(String[] args) {
		
		final List<Person> people=Arrays.asList(new Person("Zahid"),new Person("Vijay"));
		//Method Reference
		people.forEach(Person::getPrint);
		//Lambda
		people.forEach(person -> person.getPrint());
		//Normal
		for(final Person p:people) {
			p.getPrint();
		}
	
		
	}
	

	private static class Person{
		private final String name;
		
		public Person(final String _name) {
			this.name=_name;
		}
		
		public void getPrint() {
			System.out.println(name);
		}
	}

}

```
This calls the method Person.getName for each Person object in the list. Person is the particular type, and the arbitrary object is the instance of Person that is used during each loop. This looks very similar to a reference to a static method, but the difference is how the object is passed to the method reference. Remember, a static reference passes the current object into the method, whereas an arbitrary method reference invokes a method onto the current object.


**4.Reference to a constructor:**
    
  **Syntax** Class::new	
    
  **Method Reference** String::new	
    
  **Lambda expression**  () -> new String()

**Example**

```
public class ConstructorMethodReference {

	public static void main(String[] args) {
        final List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //Method Reference
        copyElements(null,ArrayList<Integer>::new);
        //Lambda Expression
        copyElements(list,() -> new ArrayList<Integer>());

	}

	private static void copyElements(final List<Integer> list, final Supplier<Collection<Integer>> targetCollection) {
		list.forEach(targetCollection.get()::add);
	}

}
```

This is the example I had the most trouble trying to make, as no matter how hard I thought, I couldn’t think of a way this could be used in something complicated. I am sure my opinion would change if I used Java 8 while at work, but for now, I do not see why this type of method reference is particularly useful. The example uses the Supplier functional interface to pass Integer::new into the copyElements method.

### Predicate vs Function in Java 8
https://www.topjavatutorial.com/java-8/predicate-vs-function-in-java-8/
**Predicate**
This is represented as Predicate<T> where T is an object type input.The purpose of Predicate<T> is to decide if the object of type T passes some test.
*Predicate interface contains a method test() , which takes one argument and returns a Boolean value.*

**Example**

Here is an example of using Predicate to determine if a number is greater than zero or not.
```
import java.util.function.Predicate;
 
public class PredicateInterfaceDemo {
 
    public static void main(String[] args) {
        
        int num = -10;
 
        Predicate gt_lt = i -> i>0 ;
 
        boolean result = gt_lt.test(num);
        
        System.out.println(num + " greater than zero : "+ result);
    }
 
}
```

Running this program will produce : -10 greater than zero : false

In the above program, a predicate gt_lt was created to test whether a number is greater than zero. Then it was tested with -10 and the test() method of Predicate returned false.

**Function**

Function interface is represented as Function<T,R> where T is the type of object passed to Function and R is the type of object returned. 
*Functions are similar to Predicates, but Functions return an object as result while Predicates return Boolean.* 
 - Function interface contains a method called apply() , which takes a one argument and returns an object as result.

**Example**

Here is an example of Function that takes a string and returns its length.
```
public class FunctionInterfaceDemo {
 
    public static void main(String[] args) {
        
        Function<String, Integer> length = str -> str.length();
        
        System.out.println("Length of string 'Hello World' is " + length.apply("Hello World"));
 
    }
 
}
```
Running this program will produce following output : Length of string ‘Hello World’ is 11

# Lambda Function by Venkat Subramaniam

To avoid annonymous class lambda expression came. A function has four things name, parameter list, body and return type. 
The most important part of any method is body and parameter list. So Lambda expression keeps only parameter list and body.
Lambda keeps parameter list in left and then notation -> and in right side body only.
```
Thread t=new Thread(() -> System.out.println("Hi"));
```
*Lambda is backward comptable, you can remove existing annonymous class with lamda in existing code, you need to write some functional interface*
If you move in java 8 your code will reduce with lambda expression. If you use annonymous inner class you will have more class created in jar.
jar size will increase , garbage collection load increases. Compiler does not replace Lambda into annonymous class. you can check by calling javap Class.class.

External Iterator: what we used in java before java 8 like for loop, do while, enhance for loop, etc.. you can controll on iteration thru external iterator.

Internal Iteraor: invoking the iteration on object. You dont have controll over internal iterator. 

```
numbers.forEach((Integer n) -> System.out.println(n));
```
You can write above code as below also, because java 8 has type inference, finally holds the tweets, but only for lambda expression. Java is intelligent to 
identify the type i.e. caled type inference
```
numbers.forEach((n) -> System.out.println(n));
```
Again there is one more enhanced in above code is , you dont require parenthesis if only one parameter in lambda.
```
numbers.forEach(n -> System.out.println(n));
```
Now in below code we just replace the lambda with method reference, in below code we given instruction to jvm get the data from list object through forEach
and pass this value to println method of System.out to display the value.
```
numbers.forEach(System.out:println);
```
*Lambda expression should be glue code. Two lines may be to many to avoid noisy, hard to test, duplicacy and hard to read*

Method reference is used when you pass what you receive to next journey.


**There are two limitation of Method reference , but work arround also available**
 - If you have some operation like business logic or calculation then Method reference should not use.
 - If you have two same method but one is static and one is instance method then compiler get confused which should call by Method reference.

stream() is a nice iterator,

funtion composition: when you compose number of pipeline using stream iterator.

parallelstream performance is 5x faster compare to stream. but parallelstream should be used only when its really required like in case of multiple thread execution.

*Stream is as abstraction. Stream in not a physical object. data does not stored in stream. its non immutable pipeline. data only flow from sream pipeline.*
Stream Functions:

filter : take value into stream and block some and go some thru.. like numbers.stream().filter(e-> e%2 == 0) this means blocking non even number and even number go thru.

map: map is a transforming function, transform values. there is no gurantee that type of input will be same at the time of output. but the number of input will be same at the time of output.
example number.stream().filter(e-> e%2==0).map(e->e*2.0) the input is conveted here to double.

sum: sum is a reduce operation. reduce operation cuts the cross swimline. 

collect: collect is also a reduce operation, we use below toList method to convert steam to list.
```
List<Integer> doubleOfEven=
    numbers.stream()
    .filter(e -> e%2 ==0)
    .map(e ->e*2)
    .collect(toList));
    System.out.println(doubleOfEven);
```

There are many Collectors method available for conversion.


collect provide toSet() method which remove the ducplicates.

toMap: to convert list to map or any conversion to map thru stream.

groupingBy: 


# Java Stream API for Bulk Data Operations on Collections:

**Stream is a best feature of java 8 available in java.util.stream package. Stream is a pipeline to flow the collections or array, stream also provudes number 
of object to perform operation into it. Stream is not an object stream does not store any value. Resonsiblity of stream is only to receive some input from
collection object then pass it to next method for operations.**

 Stream can be created from different elements sources such as collection or array. Stream is a default method added in Collection interface.
 
 Example: printing the number from 1 to 10 with two filters first number should be greater than 5 and must be divisible by 2.
 ```
 public class StreamTest {
	public static void main(String[] args) {
		List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		list.stream().filter(a -> a>5).filter(a -> a%2==0)
		.forEach(System.out::println);
	}
}
 ```
```
output
6
8
10
```

#### Multithreading with Stream.

You can do multithreading with stream using parallelstream() method, this method also introduced in Collection interface in java 8. parallelstream methods
that runs operations over stream's element in parallel mode. See the below example where a method greetUser() is running in parallelstream mode.

Example
```
public static void main(String[] args) {
		// TODO Auto-generated method stub
		ParallelStream a=new ParallelStream();
		List<String> userList=Arrays.asList("Zahid","Naiyer","Vijay");
		userList.parallelStream().forEach(elements -> greetUser(elements));
	}
	
	public static void greetUser(Object list) {
		System.out.println(list);
	}
```
output: if you run the above code with stream the output will same as inserted into list, because one stream will call the greetUser() method.
but in case of parallelstream the call on greetUser is multiple so the sequnce of output will not same as inserted.
```
Naiyer
Vijay
Zahid
```

#### Stream operation devided into two parts.
**1.** intermediate operation :

The operation allow you for chaining operation which return Stream<T>

**2.** terminal operation:

This operation return a result of definite type.

 - Please note in any case if you consume stream then you can not reuse the same stream. It will give you error let see in example

**Example**

```
public class StreamOperation {
	public static void main(String[] args) {
		List<String> list=Arrays.asList("A","B","C","D","E","A","B","D");
		System.out.println(list.stream().distinct().count());  // Here distinct() method is intermediate operation and count is terminal oeration
	}
}
```

Output: 5

Now see if you try the same above code to re use the stream , you will get error like this

```
public class StreamOperation {
	public static void main(String[] args) {
		List<String> list=Arrays.asList("A","B","C","D","E","A","B","D");
		Stream<String> stream=list.stream();
		System.out.println(stream.distinct().count());
		stream.distinct().count();
	}
}
```

Output will be as below:
```
5Exception in thread "main" 
java.lang.IllegalStateException: stream has already been operated upon or closed
	at java.util.stream.AbstractPipeline.<init>(Unknown Source)
	at java.util.stream.ReferencePipeline.<init>(Unknown Source)
	at java.util.stream.ReferencePipeline$StatefulOp.<init>(Unknown Source)
	at java.util.stream.DistinctOps$1.<init>(Unknown Source)
	at java.util.stream.DistinctOps.makeRef(Unknown Source)
	at java.util.stream.ReferencePipeline.distinct(Unknown Source)
	at com.zahid.exit.planned.java8.features.StreamOperation.main(StreamOperation.java:12)
```
**Example :** use intermediate operation anyMatch

```
public class StreamOperation {
	public static void main(String[] args) {
		List<String> list=Arrays.asList("Zahid","Shahid","Muzahid","Nahid","Zahil","Kaphil","Sahil","D");
		System.out.println(list.stream().anyMatch(a -> a.equalsIgnoreCase("zahid")));
	}
}
```

Output: true


**filter()** method allow us to pick stream of elements which satisfy a predicate.

**Example of filter**

```
public class StreamFilter {
	public static void main(String[] args) {
			List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
			list.stream().filter(a -> a>5).forEach(System.out::println);			
	}
}

```
Output

6
7
8
9
10




## Parallel Stream:

Parallel Stream is used when you want to use multithread during stream operation. Parallel Stream provides feature to run multiple execution parallaly.

**Example :**

```
public class ParallelStream {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ParallelStream a=new ParallelStream();
		List<String> userList=Arrays.asList("Zahid","Naiyer","Vijay");
		userList.parallelStream().forEach(elements -> greetUser(elements));
	}
	
	public static synchronized void  greetUser(Object list) {
		System.out.println(list);
	}

}
```
 - If you run parallelstream then output sequence will not be guranteed because multiple execution done through multithread


Output: 

Naiyer
Vijay
Zahid








































### 

### Java Time API:

### Collection API improvements:

### Concurrency API improvements:

### Java IO improvements:

### Miscellaneous Core API improvements:



# Java 1.7 Features
## New Features


# Java 1.6 Features
## New Features

# Java 1.5 Features
## New Features
