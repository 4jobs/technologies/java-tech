# Multithreading
#### History: 
 In the old days a computer had a single CPU, and was only capable of executing a single program at a time. After some time multitasking came, which meant that
 computers could execute multiple programs at the same time.
 But multitasking came with new challenges for software developers. Programs can no longer assume to have all the CPU time available, nor all memory or any 
 other computer resources. For a application program should release all resources if it is no longer using, so other programs can use them.
 That's why multithreading introduced which mean that you could have multiple **threads** of execution inside the same program.
 
 ***A thread is an independent path of execution within a program. Many threads can run concurrently within a program.***

**Defn:**

A thread is a lightweight subprocess, the smallest unit of processing. It is a separate path of execution. Threads are independent. If there occurs 
exception in one thread, it doesn't affect other threads. It uses a shared memory area.

![alternativetext](../../images/multithreading/1.PNG)

As shown in the above figure, a thread is executed inside the process. There is context-switching between the threads. There can be multiple processes 
inside the OS, and one process can have multiple threads.

## Thread State Cycle:
According to sun, there is only 4 states in thread life cycle in java new, runnable, non-runnable and terminated.
But for better understanding the threads, we are explaining it in the 5 states. And these life cycle are controlled by jvm.

 **- New:** The thread is in new state if you create an instance of Thread class but before the invocation of start() method.
   
 **- Runnable:** The thread is in runnable state after invocation of start() method, but the thread scheduler has not selected it to be the running thread.
   
 **- Running:** The thread is in running state if the thread scheduler has selected it.
   
 **- Non-Runnable (Block):** This is the state when the thread is still alive, but is currently not eligible to run.
   
 **- Terminated:** A thread is in terminated or dead state when its run() method exits.


![alternativetext](../../images/multithreading/2.PNG)

## Thread Priorities:

Execution of multiple threads on a single CPU, in some order, is called scheduling. The Java runtime supports a very simple, deterministic scheduling 
algorithm known as fixed priority scheduling. This algorithm schedules threads based on their priority relative to other runnable threads.

When a Java thread is created, it inherits its priority from the thread that created it. You can also modify a thread's priority at any time 
after its creation using the setPriority method. 

##### Priorities of Thread:
 - NORM_PRIORITY (a constant of 5) by default every thread is given priority.
 - MIN_PRIORITY (a constant of 1)
 - MAX_PRIORITY (a constant of 10).

Thread priorities are integers ranging between MIN_PRIORITY and MAX_PRIORITY (constants defined in the Thread class). The higher the integer,
the higher the priority. At any given time, when multiple threads are ready to be executed, the runtime system chooses the runnable thread with 
the highest priority for execution. Only when that thread stops, yields, or becomes not runnable for some reason will a lower priority thread 
start executing. If two threads of the same priority are waiting for the CPU, the scheduler chooses one of them to run in a round-robin fashion. 

## How to create thread:

There is only one way by which a Thread can be created in java using java.lang.Thread class as shown below,
Thread thread1 = new Thread(); 

After creating a Thread object, a separate independent path is created but what task this independent path will execute?

## How many ways are there to assign task to a thread?

There are mainly 3 ways by which Task can be assigned to a Thread either by,


#### 1. java.lang.Runnable 
#### 2. java.lang.Thread class itself.
#### 3. java.util.concurrent.Callable Interface.

### Thread vs Runnable
 **1.** Implementing Runnable is the preferred way to do it. Here, you’re not really specializing or modifying the thread’s behavior. 
        You’re just giving the thread something to run. That means composition is the better way to go.
        
 **2.** Java only supports single inheritance, so you can only extend one class.
 
 **3.** Instantiating an interface gives a cleaner separation between your code and the implementation of threads.

 **4.** Implementing Runnable makes your class more flexible. If you extend Thread then the action you’re doing is always going to 
       be in a thread. However, if you implement Runnable it doesn’t have to be. You can run it in a thread, or pass it to some kind of executor service, 
       or just pass it around as a task within a single threaded application.
       
 **5.** In Object oriented programming extending a class generally means adding new functionality, modifying or improving behaviors. If we are not making 
       any modification on Thread than use Runnable interface instead.
       
 **6.** Runnable interface represent a Task which can be executed by either plain Thread or Executors or any other means. so logical separation of 
       Task as Runnable than Thread is good design decision.
      
 **7.** Java designer recognizes this and that's why Executors accept Runnable as Task and they have worker thread which executes those task.
 
### Runnable vs Callable

 **1.** Runnable is the core interface provided for representing multi-threaded tasks and Callable is an improved version of Runnable that was added in Java 1.5.
 
 **2.** The Runnable interface is a functional interface and has a single run() method which doesn’t accept any parameters and does not return any values.
        Runnable is suitable for situations where we are not looking for a result of the thread execution, for example, incoming events logging:
        But Callable interface is a generic interface containing a single call() method – which returns a generic value V.

**3.** In Runnable the method signature does not have the “throws” clause specified, there is no way to propagate further checked exceptions.
       Callable’s call() method contains “throws Exception” clause so we can easily propagate checked exceptions further.
       
**You can also convert Runnable to Callable by using the following utility method provided by Executors class**

Callable callable = Executors.callable(Runnable task);

**4.** There is one limitation while using Callable interface in Java that you cannot pass it to Thread as you pass the Runnable instance. There is no
       defined in the Thread class which accepts a Callable interface. So in order to execute a Callable instance you need to use the ExecutorService 
       interface of Java 5 Executor framework. This interface defines submit() method which accepts a Callable instance and return a Future object which 
       holds the result of computation as shown below:
       
       Future result = exec.submit(aCallable);
       result = holder.get();
    
***Remember, Future.get() is a blocking method and blocks until execution is finished, so you should always call this method with a timeout to avoid deadlock or livelock in your application.***

#Let's see complete example of creating a Thread and assigning task to it using,

## Thread creation and Thread task assignment :

Thread class provide constructors and methods to create and perform operations on a thread.Thread class extends Object class and implements Runnable 
interface. You can create a new thread simply by extending your class from Thread and overriding it’s run() method because Thread class implements 
Runnable interface.
The run() method contains the code that is executed inside the new thread. Once a thread is created, you can start it by calling the start() method.

### Commonly used Constructors of Thread class:
**Thread()** 
Allocates a new Thread object. This constructor has the same effect as Thread (null, null, gname), where gname is a newly generated name. Automatically generated names are of the form "Thread-"+n, where n is an integer.

**Thread(String name)** 
Allocates a new Thread object. This constructor has the same effect as Thread (null, null, name).
name is the name of the new thread

**Thread(Runnable r)** 
Allocates a new Thread object. This constructor has the same effect as Thread (null, target, gname), where gname is a newly generated name. Automatically generated names are of the form "Thread-"+n, where n is an integer.
target is the object whose run method is invoked when this thread is started. If null, this classes run method does nothing.

**Thread(Runnable r,String name)** 
Allocates a new Thread object. This constructor has the same effect as Thread (null, target, name).
target is the object whose run method is invoked when this thread is started. If null, this thread's run method is invoked.
name is the name of the new thread.

**public Thread(ThreadGroup group,String name)**
Allocates a new Thread object. This constructor has the same effect as Thread (group, null, name).
group is the thread group. If null and there is a security manager, the group is determined by SecurityManager.getThreadGroup(). 
If there is not a security manager or SecurityManager.getThreadGroup() returns null, the group is set to the current thread's thread group.
name is the name of the new thread

### Commonly used Methods of Thread class:

**public void run()**
If this thread was constructed using a separate Runnable run object, then that Runnable object's run method is called; otherwise, this method does nothing and returns.
Subclasses of Thread should override this method.

**public void start()**
Causes this thread to begin execution; the Java Virtual Machine calls the run method of this thread. 

**public static void sleep(long millis,int nanos)throws InterruptedException**
Causes the currently executing thread to sleep (temporarily cease execution) for the specified number of milliseconds plus the specified number of nanoseconds, subject to the precision and accuracy of system timers and schedulers. The thread does not lose ownership of any monitors.
millis is the length of time to sleep in milliseconds and nanos is 0-999999 additional nanoseconds to sleep

**public static void sleep(long millis) throws InterruptedException**
Causes the currently executing thread to sleep (temporarily cease execution) for the specified number of milliseconds, subject to the precision and accuracy of system timers and schedulers. The thread does not lose ownership of any monitors.
millis is the length of time to sleep in milliseconds.

**public final void join() throws InterruptedException**
Waits for this thread to die. An invocation of this method behaves in exactly the same way as the invocation join(0)

**public final void join(long millis) throws InterruptedException**
Waits at most millis milliseconds for this thread to die. A timeout of 0 means to wait forever.
This implementation uses a loop of this.wait calls conditioned on this.isAlive. As a thread terminates the this.notifyAll method is invoked. It is recommended that applications not use wait, notify, or notifyAll on Thread instances.

**public final void join(long millis,int nanos) throws InterruptedException**
Waits at most millis milliseconds plus nanos nanoseconds for this thread to die. This implementation uses a loop of this.wait calls conditioned on this.isAlive. As a thread terminates the this.notifyAll method is invoked. It is recommended that applications not use wait, notify, or notifyAll on Thread instances.

**public final int getPriority()** 
public final int getPriority().

**public final void setPriority(int newPriority)**
Changes the priority of this thread. First the checkAccess method of this thread is called with no arguments. This may result in throwing a SecurityException.
Otherwise, the priority of this thread is set to the smaller of the specified newPriority and the maximum permitted priority of the thread's thread group.

**public final String getName()**
Returns this thread's name.

**public final void setName(String name)**
Changes the name of this thread to be equal to the argument name. First the checkAccess method of this thread is called with no arguments. This may result in throwing a SecurityException.

**public static Thread currentThread()**
Returns a reference to the currently executing thread object.

**public long getId()**
Returns the identifier of this Thread. The thread ID is a positive long number generated when this thread was created. 
The thread ID is unique and remains unchanged during its lifetime. When a thread is terminated, this thread ID may be reused.

**public Thread.State getState()**
Returns the state of this thread. This method is designed for use in monitoring of the system state, not for synchronization control.

**public final boolean isAlive()**
Tests if this thread is alive. A thread is alive if it has been started and has not yet died. true if this thread is alive; false otherwise.

**public static void yield()**
causes the currently executing thread object to temporarily pause and allow other threads to execute.

**public final void suspend()**
This method has been deprecated, as it is inherently deadlock-prone. If the target thread holds a lock on the monitor protecting a critical system 
resource when it is suspended, no thread can access this resource until the target thread is resumed. If the thread that would resume the target thread 
attempts to lock this monitor prior to calling resume, deadlock results. Such deadlocks typically manifest themselves as "frozen" processes".
***If the thread is alive, it is suspended and makes no further progress unless and until it is resumed.***

**public final void resume()**
This method exists solely for use with suspend(), which has been deprecated because it is deadlock-prone.
***If the thread is alive but suspended, it is resumed and is permitted to make progress in its execution.***

**public final void stop()**
This method is inherently unsafe. Stopping a thread with Thread.stop causes it to unlock all of the monitors that it has locked, If any of the objects 
previously protected by these monitors were in an inconsistent state, the damaged objects become visible to other threads, potentially resulting in 
arbitrary behavior. This method has been deprecated.

**public final boolean isDaemon()**
Tests if this thread is a daemon thread. true if this thread is a daemon thread; false otherwise.

**public final void setDaemon(boolean on)**
Marks this thread as either a daemon thread or a user thread. The Java Virtual Machine exits when the only threads running are all daemon threads.
This method must be invoked before the thread is started.

**public void interrupt()**
Interrupting a thread that is not alive need not have any effect.

**public boolean isInterrupted()**
Tests whether this thread has been interrupted. The interrupted status of the thread is unaffected by this method.
A thread interruption ignored because a thread was not alive at the time of the interrupt will be reflected by this method returning false.
true if this thread has been interrupted; false otherwise.

**public static boolean interrupted()**
Tests whether the current thread has been interrupted. The interrupted status of the thread is cleared by this method. In other words, 
if this method were to be called twice in succession, the second call would return false (unless the current thread were interrupted again, 
after the first call had cleared its interrupted status and before the second call had examined it).
A thread interruption ignored because a thread was not alive at the time of the interrupt will be reflected by this method returning false.
true if the current thread has been interrupted; false otherwise.


### Thread Example by extending Thread class
```
class Abc extends Thread{  
public void run(){  
System.out.println("thread is running from Abc Class...");  
}  
public static void main(String args[]){  
Abc t1=new Abc();  
t1.start();  
 }  
}  
```


Above code performs following tasks:
 - A new thread starts(with new callstack).
 - The thread moves from New state to the Runnable state.
 - When the thread gets a chance to execute, its target run() method will run. run method is present in Runnable interface and Thread implements Runnable

```
thread is running from Abc Class...
```

# Task assigned to Runnable from Thread object:
Runnable interface is the primary template for any object that is intended to be executed by a thread. It defines a single method run(), which is meant to contain the code that is executed by the thread.
Any class whose instance needs to be executed by a thread should implement the Runnable interface.
The Thread class itself implements Runnable with an empty implementation of run() method.

For creating a new thread, create an instance of the class that implements Runnable interface and then pass that instance to Thread(Runnable target) constructor.

### Thread Example by implementation of Runnable interface
```
class Xyz implements Runnable{  
public void run(){  
System.out.println("thread is running from Xyz...");  
}  

public static void main(String args[]){  
Xyz m1=new Xyz();  
Thread t1 =new Thread(m1);  
t1.start();  
 }  
}   
```

***If you are not extending the Thread class,your class object would not be treated as a thread object.So you need to explicitely create Thread class object.We are passing the object of your class that implements Runnable so that your class run() method may execute.***


```
thread is running from Xyz Class...
```

### Thread Example by implementation of Callable interface

The thread is a separate path of execution, so if you have to do a repetitive task, you can break the work into multiple chunks (tasks) and assign them 
to threads. Multiple Threads will execute tasks in parallel to get the result quickly.
In Java 5, java.util.concurrent was introduced. The callable interface was introduced in concurrency package, which is similar to the Runnable interface, 
but it can return any object, and is able to throw an Exception.

Callable interface uses Generics, so it can return any type of Object. The Executor Framework offers a submit() method to execute Callable implementations
in a thread pool. Actually, the Java Executor Framework follows WorkerThread patterns, wherein a thread pool you can initiate threads by using the  
Executors.newFixedThreadPool(10); method. Then you can submit a task to it.
In Java, a runnable acts as the target of a thread, and in the runnable interface, a  public void run() method has to be implemented where you define 
the task, which will be executed by threads in the thread pool. The Executor Framework assigns work (runnable target) to threads only if there is an 
available thread in the pool. If all threads are in use, the work has to wait. Once a task is completed by the thread, that thread returns to the pool 
as an available thread. 

Callable is same as Runnable but it can return any type of Object if we want to get a result or status from work (callable).

#### Java Future:
Future interface has methods to obtain the result generated by a Callable object and to manage its state.






















